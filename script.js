function makeTable(){    
    var table=document.getElementById('tab');
    var row=document.getElementById('rows').value;
    var col=2;


    for(var i=0;i<row;i++)
    {
        var tr=document.createElement('tr');
        for(var j=0;j<1;j++)
        {
            var td=document.createElement('input');
            td.type="number";
            td.step="0.1";
            td.setAttribute("id", "input"+i );

            var text=document.createTextNode(j);
            td.appendChild(text);
            tr.appendChild(td);

        }
        for(var j=0;j<1;j++)
        {
            var td=document.createElement('output');
            td.setAttribute("id", "out"+i );

            var text=document.createTextNode(j);
            td.appendChild(text);
            tr.appendChild(td);
                
        }

        table.appendChild(tr);
    }
    
}
document.getElementById('make').addEventListener("click",makeTable);


function cube() {
    var rang=document.getElementById('rows').value;

     for( var i = 0; i < rang; i++ ) {
        var total = 0;

        
        a=document.getElementById('input' + i).value;
        if(a==""){
            document.getElementById('out'+i).value = "Value not entered";
        }
        else{
        total = a*a*a;
        document.getElementById('out'+i).value = total.toFixed(3);
        }
    
    }
}

